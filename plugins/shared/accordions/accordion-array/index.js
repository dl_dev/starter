import { install } from "@/app/App.js";
import initAccordion from "../lib/init-accordion.js";

install(".js-accordion-array", (root) => {
  // * Скрытие/раскрытие деталей
  const matchesItems = root.querySelectorAll("[data-role='item']");
  matchesItems.forEach(initItem);

  function initItem(matchesItem) {
    const button = matchesItem.querySelector("[type='button']");
    const content = matchesItem.querySelector("[data-role='content']");

    initAccordion({
      button,
      content,
      root,
      elem: matchesItem,
      single: root.dataset.single
    });
  }

  // * Отображение/скрытие элементов
  const expandButton = root.querySelector("[data-role='expand']");
  expandButton && expandButton.addEventListener("click", () => {
    expandButton.classList.toggle("_expanded");
    root.classList.toggle("_expanded");
  });
});