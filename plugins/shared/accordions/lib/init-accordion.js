import setHeightProperty from "@/shared/_lib/set-height-property.js";

export default function initAccordion(options) {
  const {
    button,
    stateClass = "_show",
    content,
    root,
    elem
  } = options;

  setHeightProperty(content);

  button.addEventListener("click", () => {
    const activeItem = root.querySelector(`.${stateClass}`);
    if (activeItem !== elem) {
      activeItem && activeItem.classList.remove(stateClass);
    }

    elem.classList.toggle(stateClass);
    setHeightProperty(content);

  // Обновление высоты слайда, если аккордион внутри свайпера
  const swiperElem = root.closest(".swiper");
  
    if (swiperElem && swiperElem.swiper) {
      const swiper = swiperElem.swiper;
      setInterval(() => {
        swiper.update();
      }, 100)
    }
  });

  window.addEventListener("resize", () => {
    setHeightProperty(content);
  });
}
