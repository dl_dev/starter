import { install } from "@/app/App.js";

const formatNumber = (num, allowDecimal, isInput = false) => {
  const parts = num.toString().split(".");

  if (!allowDecimal) {
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
  }

  if (allowDecimal && parts[1] !== undefined) {
    return `${parts[0]}.${parts[1]}`;
  }
  return parts[0];
};

const parseNumber = (str, isStepDecimal) => {
  if (isStepDecimal) return str;
  return parseFloat(str.replace(/\s/g, "").replace(/[^0-9.]/g, ""));
};

const roundToStep = (value, step) => {
  const multiplier = 1 / step;
  return Math.round(value * multiplier) / multiplier;
};

const pluralizeUnit = (num, unit) => {
  const forms = unit.split(", ");
  if (forms.length === 1) return forms[0];
  if (num % 10 === 1 && num % 100 !== 11) return forms[0];
  if (num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20)) return forms[1];
  return forms[2];
};

const updateInputWidth = (input) => {
  const tempSpan = document.createElement("span");
  tempSpan.style.fontSize = window.getComputedStyle(input).fontSize;
  tempSpan.style.fontFamily = window.getComputedStyle(input).fontFamily;
  tempSpan.style.visibility = "hidden";
  tempSpan.style.whiteSpace = "nowrap";
  tempSpan.textContent = input.value || input.placeholder;
  document.body.appendChild(tempSpan);
  input.style.width = `${tempSpan.offsetWidth + 3}px`;
  document.body.removeChild(tempSpan);
};

install(".js-field-range", (field) => {
  const inputNumber = field.querySelector("[type='text']");
  const inputRange = field.querySelector("[type='range']");
  const unitElement = field.querySelector("[data-role='unit']");
  const dataset = field.dataset;
  const { unit, min, max, step, value } = dataset;
  const isStepDecimal = step % 1 !== 0; // Проверяем, является ли шаг дробным

  const updateValue = (value, format = true, round = false, isInput = false) => {
    let val = parseNumber(value, isStepDecimal);
    if (isNaN(val) || value === "") {
      val = ""; // Разрешаем пустое значение
    } else {
      if (round && step) val = roundToStep(val, parseFloat(step)); // Округляем до ближайшего шага
      if (val > max) val = max; // Проверяем на максимальное значение
    }

    const formattedValue = format ? formatNumber(val, isStepDecimal, isInput) : value;
    inputNumber.value = formattedValue;
    inputNumber.setSelectionRange(inputNumber.value.length, inputNumber.value.length); // Устанавливаем курсор в конец

    // * вставка единицы измерения
    if (unit) {
      const unitText = unit.includes(",") ? pluralizeUnit(val, unit) : unit;
      unitElement.innerHTML = unit === "%" ? unitText : `&nbsp;${unitText}`;
    }

    inputRange.value = (val === "") ? min : val; // устанавливаем значение в шкале
    renderScaleFill();
  };

  // Отрисовка градиентом позиции шкалы
  const renderScaleFill = () => {
    const scaleMax = max - min;
    const value = inputRange.value - min;
    const percentage = (100 / scaleMax) * value;

    inputRange.style.backgroundSize = `${percentage}% 100%`;
  };

  const handleInputChange = () => {
    let inputValue = inputNumber.value;

    // Разрешаем только цифры, точку и запятую (заменяем запятую на точку)
    inputValue = inputValue.replace(/,/g, ".").replace(/[^\d.]/g, "");

    // Запрещаем более одной точки
    const decimalCount = (inputValue.match(/\./g) || []).length;
    
    if (decimalCount > 1) {
      inputValue = inputValue.slice(0, inputNumber.selectionStart - 1) + inputValue.slice(inputNumber.selectionStart);
    }

    updateValue(inputValue, true, false, true); // Форматируем во время ввода, но не округляем и не устанавливаем разрядность
    updateInputWidth(inputNumber); // Обновляем ширину инпута
  };

  const handleRangeInput = () => {
    updateValue(inputRange.value);
    updateInputWidth(inputNumber);
  };

  const handleInputBlur = () => {
    const value = parseNumber(inputNumber.value, isStepDecimal);
    if (value < min || isNaN(value)) {
      updateValue(min);
    } else {
      updateValue(inputNumber.value, true, true); // Форматируем и округляем при blur
    }
    updateInputWidth(inputNumber);
  };

  // Устанавливаем начальное значение
  inputRange.value = value;
  updateValue(value);
  updateInputWidth(inputNumber);

  // Навешиваем обработчики
  inputNumber.addEventListener("input", handleInputChange);
  inputNumber.addEventListener("blur", handleInputBlur);
  inputRange.addEventListener("input", handleRangeInput);
  renderScaleFill();
});

