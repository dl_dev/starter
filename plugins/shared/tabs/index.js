import App from "@/app/App.js";
import Swiper from "swiper";
import { EffectFade } from "swiper/modules";

App.install(".js-tabs", (tabList) => {
  // Инициализация слайдера
  let tabSlider = null;

  if (tabList.classList.contains("swiper")) {
    tabSlider = initTabSwiper(tabList);
  } else {
    tabSlider = initSimpleTabs(tabList);
  }

  // Навигация по табам
  const id = tabList.dataset.id;
  const navigationList = document.querySelectorAll(`.js-tab-nav[data-bind="${id}"]`);
  navigationList.forEach((navElem) => {
    initTabNavigation(navElem, tabSlider);
  });
});

function initTabSwiper(elem) {
  return new Swiper(elem, {
    modules: [EffectFade],
    allowTouchMove: false,
    effect: "fade",
    fadeEffect: {
      crossFade: true
    },
    autoHeight: true
  });
}

// * Простые табы для случаев, когда внутри табов есть слайдеры
function initSimpleTabs(elem) {
  const tabs = elem.querySelectorAll(".tab-content");

  function slideTo(index) {
    tabs.forEach((tab, i) => {
      if (i === index) {
        tab.classList.add("_active");
      } else {
        tab.classList.remove("_active");
      }
    });
  }

  return {
    slideTo
  };
}

function initTabNavigation(navElem, tabSlider) {
  let currentSlide = 0;
  const buttons = navElem.querySelectorAll("button");
  const carrete = navElem.querySelector("[data-role='carrete']");

  const toSlide = (index) => {
    if (currentSlide >= 0) {
      buttons[currentSlide].disabled = false;
    }

    currentSlide = index;
    buttons[index].disabled = true;
    tabSlider.slideTo(index);

    // Двигаем каретку
    if (carrete) {
      const targetButton = buttons[index];
      const buttonRect = targetButton.getBoundingClientRect();
      const buttonWidth = targetButton.offsetWidth;
      const navElemRect = navElem.getBoundingClientRect();
      const offsetX = buttonRect.left - navElemRect.left;
      carrete.style.width = `${buttonWidth}px`;
      carrete.style.left = `${offsetX}px`;
    }
  };

  

  toSlide(0);

  buttons.forEach((button, index) => {
    button.addEventListener("click", () => {
      toSlide(index);
    });
  });

  if (carrete) {
    const updateCarretePosition = () => {
      if (currentSlide !== undefined) {
        const targetButton = buttons[currentSlide];
        const buttonRect = targetButton.getBoundingClientRect();
        const buttonWidth = targetButton.offsetWidth;
        const navElemRect = navElem.getBoundingClientRect();
        const offsetX = buttonRect.left - navElemRect.left;
        carrete.style.width = `${buttonWidth}px`;
        carrete.style.left = `${offsetX}px`;
      }
    };
    // Обновляем позицию каретки при изменении размера окна
    window.addEventListener("resize", updateCarretePosition);
  }
}
