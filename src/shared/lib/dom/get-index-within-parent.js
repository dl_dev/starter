// Возвращает индекс выбранного элемента относительно его родителя

export default function getIndexWithinParent(element) {
  return Array.prototype.indexOf.call(element.parentNode.children, element);
}
