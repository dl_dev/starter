export default function setHeightProperty(elem, correction = 0) {
  elem.style.setProperty("--height", "0px");
  const children = [...elem.children];
  const height = children.reduce((sum, item) => sum + item.offsetHeight, 0)
  elem.style.setProperty("--height", height + correction + "px");

  // * доп. корректировка если внутри слайдера
  setTimeout(() => {
    const swiperElem = elem.closest(".swiper");
    if (swiperElem && swiperElem.swiper) {
      swiperElem.swiper.updateSize(300);
    }
  }, 100)
}