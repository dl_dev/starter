const initializedElements = new WeakMap();

const App = {
  /**
   * Инициализирует элементы по селектору с помощью переданной функции или класса.
   * @param {string} selector - CSS-селектор для поиска элементов.
   * @param {Function} constructor - Функция или класс для инициализации элемента.
   * @param {Object} [options={}] - Опции для передачи в конструктор.
   * @param {boolean} [useClass=false] - Использовать ли `new` для создания экземпляра.
   * @returns {Array} Массив инициализированных объектов.
   */
  install(selector, constructor, options = {}, useClass = false) {
    const elems = document.querySelectorAll(selector);
    if (elems.length === 0) return [];
    const initializedObjs = [];
    elems.forEach(elem => {
      if (initializedElements.has(elem)) return;
      try {
        const instance = useClass ? new constructor(elem, options) : constructor(elem, options);
        initializedElements.set(elem, instance);
        initializedObjs.push(instance);
      } catch (e) {
        console.error(`Error initializing element: ${e.message}`, elem);
      }
    });
    return initializedObjs;
  },

  /**
   * Загружает внешний скрипт с поддержкой промисов и дополнительных атрибутов.
   * @param {Object} config - Конфигурация загрузки.
   * @param {string} config.path - Путь к скрипту.
   * @param {Function} [config.onload] - Callback при успешной загрузке.
   * @param {Function} [config.onerror] - Callback при ошибке.
   * @param {boolean} [config.async=true] - Атрибут async.
   * @param {boolean} [config.defer=false] - Атрибут defer.
   * @returns {Promise} Промис, разрешающийся при загрузке скрипта.
   */
  loadScript({ path, onload, onerror, async = true, defer = false }) {
    return new Promise((resolve, reject) => {
      if (document.querySelector(`script[src="${path}"]`)) {
        resolve();
        return;
      }
      const script = document.createElement("script");
      script.src = path;
      script.async = async;
      script.defer = defer;
      script.addEventListener("load", () => {
        onload?.();
        resolve();
      });
      script.addEventListener("error", () => {
        console.error(`Error loading script: ${path}`);
        onerror?.();
        reject(new Error(`Failed to load script: ${path}`));
      });
      document.body.appendChild(script);
    });
  },

  /**
   * Инициализирует приложение, добавляя его в window.App.
   * @param {boolean} [force=false] - Принудительно перезаписать window.App.
   */
  init(force = false) {
    if (!window.App || force) {
      window.App = this;
    }
  },
};

export default App;
export const { install, loadScript } = App;