import { installClass } from "@/app/App";
import AnimatedSection from "./lib/AnimatedSection";

installClass(".js-animated-section", AnimatedSection, {
  // repeatable: true,
  // fired: false,
  // threshold: 0.2,
  // onVisible() {},
  // onHide() {},
});