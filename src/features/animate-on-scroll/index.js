import Aos from "aos";

Aos.init({
  offset: (matchMedia('(max-width: 576px)').matches) ? 64 : 200,
  duration: 700,
  once: true,
});