import "./calculations/index.js"; // Вычисления и запись в глоб. переменные
import "./adaptive-move/index.js"; // Перенос элементов по брейкпойнту для избежания дублирования кода
import "./fancybox/index.js"; // Галерея Fancybox