const params = {
  showSelector: ".js-show-modal",
  closeSelector: ".js-close-modal",
  overlaySelector: ".js-overlay",
  bodyStateClass: "_show-modal",
  modalStateClass: "_show",
  modalSelector: ".js-modal",
};

let isModalActive = false;
let activeModal = null;
let previousModal = null;

const showElems = document.querySelectorAll(params.showSelector);
const overlay = document.querySelector(params.overlaySelector);
const closeElems = document.querySelectorAll(params.closeSelector);
const modals = document.querySelectorAll(params.modalSelector);

if (showElems.length && overlay && closeElems.length && modals.length) {
  init();
}

function init() {
  document.addEventListener("click", (event) => {
    const closeElem = event.target.closest(params.closeSelector);
    closeElem && closeModal();

    const showElem = event.target.closest(params.showSelector);
    if (showElem) {
      event.preventDefault();

      const modalId = showElem.dataset.target;
      if (!modalId) return;

      const modal = document.getElementById(modalId);

      showModal(modal);

      if (!modal) {
        throw new Error(`Не найден элемент модального окна с id ${modalId}`);
      }
    }
  });

  overlay.addEventListener("click", () => {
    closeModal();
  });
}

function showModal(elem) {
  if (typeof elem === "string") {
    elem = document.getElementById(elem);
  }

  // Закрыть активную модалку, если она существует
  if (activeModal) {
    previousModal = activeModal;
    activeModal.classList.remove(params.modalStateClass);
  }

  elem.classList.add(params.modalStateClass);
  document.body.classList.add(params.bodyStateClass);

  activeModal = elem;
  isModalActive = true;

  const input = elem.querySelector("input");
  input?.focus();
}

function closeModal() {
  if (activeModal) {
    activeModal.classList.remove(params.modalStateClass);
    document.body.classList.remove(params.bodyStateClass);
    activeModal = null;
    isModalActive = false;

    if (previousModal) {
      showModal(previousModal);
      previousModal = null;
    }
  }
}

// Закрытие окна по Esc
window.addEventListener("keydown", (event) => {
  event.key === "Escape" && isModalActive && closeModal();
});

// Вынос в глобал
window.initModals = init;
window.showModal = showModal;
window.closeModal = closeModal;
