import App from "./app/App.js";
import "./shared/index.js"
import "./features/index.js";
import "./widgets/index.js";

App.init();