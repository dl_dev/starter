import { existsSync, copyFileSync } from 'fs';
import { dirname, join } from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const srcPath = join(__dirname, 'gulp', 'config', '!ftp.js');
const destPath = join(__dirname, 'gulp', 'config', 'ftp.js');

if (!existsSync(destPath)) {
  copyFileSync(srcPath, destPath);
}