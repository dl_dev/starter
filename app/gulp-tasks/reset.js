import { deleteAsync } from "del";

export const reset = async (cb) => {
	console.log('Cleaning path:', app.path.clean); // Для отладки
	await deleteAsync(app.path.clean, {force: true});
	cb();
}