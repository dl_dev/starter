import tinyPNG from "gulp-tinypng-compress";
import keys from "../config/keys.js";

export const tiny = () => {
	return app.gulp.src(app.path.raw.img)
		.pipe(tinyPNG({
			key: keys.tinyPNG,
			sigFile: 'images/.tinypng-sigs',
			log: true
		}))
		.pipe(app.gulp.dest(`${app.path.prepare.img}`));
}